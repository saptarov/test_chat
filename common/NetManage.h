#ifndef ANETMANAGE_H
#define ANETMANAGE_H
#include "common.h"
#include "protoChat.h"
/*
 * @brief class for manipulate TCP\IP
*/

enum class EHelpType                     // type to help
{
    ALL,
    REG,
    USRLST,
    USR,
    SND
};
class ANetManage
{

public:
    uint8_t header[HEADER_LENGTH];   // header of data proto
protected:
    int tSocket;
    std::vector<uint8_t> dataPack;          //using for write userdata (loginname, message, id and other)
public:
    /*
     * @brief initializing client class
     * @host     -   is ip address           required
     * @port     -   is a system port        required
    */
    bool connToSrv(const char * host="127.0.0.1",int port=9995);
    /*
     * @brief start to listing
     * @host     -   is ip address           required
     * @port     -   is a system port        required
    */
    bool startServer(const char * host="127.0.0.1",int port=9995);
    /*
     * s@brief sender to server response
     * @data     -   data for send           required
    */
    bool sendBuf(int ptSocket,std::vector<uint8_t> &data);
    /*
     * @brief sender to server response
     * @data     -   data for write userdata           not required
    */
    bool readBuf(int ptSocket,std::vector<uint8_t> &data);
    /*
     * @brief wait for ending working
    */
    void update();
    /*
     * @brief recieved data handler
    */
    virtual bool handler(int clientId)=0;
    /*
     * @brief return int value from header (0x00 XX XX XX XX 0x00 - where XX byte of int value)
     * @bError return true if error required
    */
    size_t getLenOfMessage(bool &bError);
    /*
     * @brief write int value to header
     * @value reference of int data required
    */
    void setLenOfMessage(int &value);
    /*
     * @brief prepare and send message to clientId socket
     * @clientID socket number of pipe  not required
     * @message typing command   required
    */
    bool sendMessage(std::string message,int clientID = -1);
};

#endif // ANETMANAGE_H
