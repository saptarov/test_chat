#ifndef PROTOCHAT_H
#define PROTOCHAT_H

#include <iostream>
#include <iterator>
#include <limits>
#include <algorithm>
#include <vector>
#define MAX_BUFFER_LENGTH 1024
#define HEADER_LENGTH 4
#define BUFFER_INT 4
#define vuint8(buf) std::vector<uint8_t>(std::begin(buf),std::end(buf))

/*
 * helped funcs,procs
*/
namespace util
{
    static void toByte(int &value, uint8_t b[BUFFER_INT])
    {
        union//converter union
        {
            uint8_t b[BUFFER_INT];
           int v;
        }t;
        t.v = value;
        std::copy(std::begin(t.b),std::end(t.b),b);
    }
    static void toByte(uint &value, std::vector<uint8_t>& b)
    {
        union
        {
            uint8_t b[BUFFER_INT];
           uint v;
        }t;
        t.v = value;
        b.clear();
        b.insert(b.begin(),std::begin(t.b),std::end(t.b));
    }
    static void toByte(int &value, std::vector<uint8_t>& b)
    {
        union
        {
            uint8_t b[BUFFER_INT];
            int v;
        }t;
        t.v = value;
        b.clear();
        b.insert(b.begin(),std::begin(t.b),std::end(t.b));
    }
    static void toInt32(std::vector<uint8_t> value, int &b)
    {
        union
        {
            uint8_t b[BUFFER_INT];
           int v;
        }t;
        std::copy(std::begin(value),std::end(value),t.b);
        b = t.v;
    }
}
#endif // PROTOCHAT_H
