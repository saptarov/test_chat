#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <string>
#include <memory>
#include <algorithm>
#include <vector>
#include <map>
#include <chrono>
#include <atomic>
#include <thread>
#include <mutex>
#include <nlohmann/json.hpp>

#ifndef _WIN32
#include <netinet/in.h>
# ifdef _XOPEN_SOURCE_EXTENDED
#  include <arpa/inet.h>
# endif
#endif
#include <sys/socket.h>

/* win32 is not have shortnamed unsigned int*/
#ifdef __WIN32
    using uint = unsigned int;//may be use  uint32_t type
#endif

struct arl
{
    static void writeLog(const char* fmt)
    {
        std::cout<<fmt;
    }
    template<typename T,typename ...Args>
    static void writeLog(const char* fmt, T t,Args ...args)
    {
        for(;*fmt!='\0';fmt++)
        {
            if(*fmt == '{' && *(fmt+1) == '}') {

                std::cout<<t;
                return writeLog(fmt+2,args...);
            }
            std::cout<<*fmt;
        }
    }

    template<typename ...Args>
    static bool error(const char* filename,const char* fnc,int line, const char* fmt, Args ...args)
    {
        if(fmt == nullptr)
            return false;
        std::string path = filename;
        path = path.substr(path.find_last_of('/') + 1);
        std::cout<<"===========ERROR=============\n";
        std::cout<<path<<"::";
        std::cout<<fnc<<"(";
        std::cout<<line<<")\n";
        writeLog(fmt, args...);
        std::cout<<"\n===========================\n"<<std::endl;
        return false;
    }
    template<typename ...Args>
    static void info(const char* fmt, Args ...args)
    {
        if(fmt == nullptr)
            return;
        std::cout<<"INFO: ";
        writeLog(fmt, args...);
        std::cout<<std::endl;
    }
    static void trace(const char* filename,const char* fnc,int line)
    {
        std::cout<<"=========== TRACE =============\n";
        std::cout<<"FILE: "<<filename<<"\n";
        std::cout<<"FUNCTION: "<<fnc<<"\n";
        std::cout<<"LINE: "<<line<<"\n";
        std::cout<<"===============================\n";
        std::cout<<std::endl;
    }
    static bool isCorrectData(std::vector<std::string> tags,nlohmann::json &j) {
        for(std::string tag : tags) {
            if(j.find(tag) == j.end())
                return false;
        }
        return true;
    }
};

#define arl_error(f_,...)({ \
    arl::error(__builtin_FILE(),__builtin_FUNCTION(),__builtin_LINE(), f_, ## __VA_ARGS__ ); \
    false; \
})
#define arl_trace(f_,...)({ \
    arl::trace(__builtin_FILE(),__builtin_FUNCTION(),__builtin_LINE()); \
})

#endif // COMMON_H
