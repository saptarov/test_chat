#include "NetManage.h"

bool ANetManage::sendBuf(int ptSocket,std::vector<uint8_t> &data)
{
    std::vector<uint8_t> buffer;
    int len = data.size();
    setLenOfMessage(len);//set header data
    buffer.insert(buffer.begin(),std::begin(header),std::end(header));
    if(data.size()>0)
        buffer.insert(buffer.begin()+HEADER_LENGTH,data.begin(),data.end());
    std::vector<uint8_t> shower;
    shower.insert(shower.begin(),std::begin(buffer),std::end(buffer));
    ssize_t bufSize = send(ptSocket, buffer.data(), MAX_BUFFER_LENGTH, 0 );
    if(bufSize<1) {
        arl_error("Failed to send, connection closed");
        return false;
    }
    arl::info("send {}",bufSize);
    return true;
}

bool ANetManage::readBuf(int ptSocket,std::vector<uint8_t> &data)
{
    uint8_t buffer[MAX_BUFFER_LENGTH];
    ssize_t bufSize = recv(ptSocket, buffer, MAX_BUFFER_LENGTH,0);
    bool bError = false;
    std::copy(std::begin(buffer),std::begin(buffer)+HEADER_LENGTH,header);
    int len = getLenOfMessage(bError);

    if(bError)
        return false;
    if(bufSize< 1) {
        arl_error("Error reciev data, connection closed");
        return false;
    }
    arl::info("readBuf");
    data.clear();
    data.insert(data.begin(),std::begin(buffer)+HEADER_LENGTH,std::begin(buffer)+(HEADER_LENGTH+len));
    return true;
}
bool ANetManage::connToSrv(const char * host,int port)
{
    tSocket = 0;
    struct sockaddr_in serv_addr;
    uint8_t buffer[MAX_BUFFER_LENGTH] = {0};
    if ((tSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        return arl_error("Socket creation error ");

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);

    // Convert to IPv4
    if(inet_pton(AF_INET, host, &serv_addr.sin_addr)<=0)
        return arl_error("Invalid address {} Address not supported",host);

    if (connect(tSocket, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        return arl_error("Connection Failed {}:{}",host,port);
    return true;
}

bool ANetManage::startServer(const char * host,int port)
{
    tSocket = 0;
    struct sockaddr_in serv_addr;
    uint8_t buffer[MAX_BUFFER_LENGTH] = {0};
    if ((tSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        return arl_error("Socket creation error ");

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);
    if(inet_pton(AF_INET, host, &serv_addr.sin_addr)<=0)
        return arl_error("Invalid address {} Address not supported",host);
    if (bind(tSocket, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) == -1)
        return arl_error("Binding failed");
    listen(tSocket,5);
    return true;
}

void ANetManage::update()
{
    std::this_thread::yield();
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
}

size_t ANetManage::getLenOfMessage(bool &bError)
{
    std::vector<uint8_t> bufftempValue;
    int buffVal=0;
    bufftempValue.insert(bufftempValue.begin(),std::begin(header),std::end(header));
    util::toInt32(bufftempValue,buffVal);
    bError = (buffVal == std::numeric_limits<int>::max() || buffVal == std::numeric_limits<int>::min() || buffVal < 0);
    return buffVal;
}
void ANetManage::setLenOfMessage(int &value)
{
    std::vector<uint8_t> bufftempValue;
    util::toByte(value,bufftempValue);
    std::copy(bufftempValue.begin(),bufftempValue.end(),header);
}

bool ANetManage::sendMessage(std::string message,int clientID)
{
    if(clientID == -1)
        clientID = tSocket;
    dataPack.resize(message.length()+BUFFER_INT);
    dataPack.insert(dataPack.begin(),message.begin(),message.end());
    return sendBuf(clientID,dataPack);
}
