#ifndef CLIENT_H
#define CLIENT_H
#include "NetManage.h"

class AClient: public ANetManage
{
    std::map<std::string,int>   m_connClients;  //recieved users from server
    std::mutex                  m_mutexHandler;
    int                         m_sessionId=-1;
    std::map<std::string,std::vector<std::string>> anotherClientMessages;
public:
    /*
     * @brief started application threads
    */
    void run();
    /*
     * @brief -- recieved data handler
    */
    bool handler(int) override;
    /*
     * @brief   registered our client in server
     * @input   typing command   required
    */
    bool reger(std::string &&input);
    /*
     * @brief   manipulate of clients array
     * @input   typing command   required
    */
    bool usrListManage(std::string &&input);
    /*
     * @brief   typing command for client
     * @cli     pointer to this class
    */
    void typeCommand();
    /*
     * @brief   helper for show in command line
    */
    void helpShow(EHelpType type);
};

#endif // CLIENT_H
