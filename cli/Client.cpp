#include "Client.h"

void AClient::run()
{
    while(1) {
        typeCommand();
        return;
        update();
    }
}

bool AClient::handler(int plug)
{
    std::lock_guard<std::mutex> l(m_mutexHandler);
    nlohmann::json j =  nlohmann::json::parse(dataPack.data());
    arl::info("reciev data {}",j.dump());
    if(!arl::isCorrectData({"id","command"},j))
        return arl_error("incorrect reciev data");

    if(j.find("command").value() == "login"){
        if(!arl::isCorrectData({"session"},j))
            return arl_error("data is not have session number ");
        m_sessionId = j.find("session").value();
    }

    if(j.find("command").value() == "message")
    {
        if(!arl::isCorrectData({"body","sender_login","session"},j))
            return arl_error("incorrect message command data");
        int senderSession = j.find("session").value();
        std::string senderLogin = j.find("sender_login").value();
        m_connClients[senderLogin] = senderSession;
        std::cout<< j.find("body").value()<<std::endl;
        anotherClientMessages[senderLogin].push_back(j.find("body").value());


        auto result = nlohmann::json::object();
        result["id"] = 1;
        result["command"] = "message_reply";
        result["status"] = "ok";
        result["client_id"] = anotherClientMessages[senderLogin].size();
        sendMessage(result.dump());
    }

    return true;
}


void AClient::typeCommand()
{
    std::string input="";
    int cnt=0;
    std::string data;
    do {
        //load data from command line
        std::getline(std::cin,input);
        size_t start;
        size_t end = 0;
        std::vector<std::string> typedCommand;

        //split command data to array of string
        while ((start = input.find_first_not_of(' ', end)) != std::string::npos) {
            end = input.find(' ', start);
            typedCommand.push_back(input.substr(start, end - start));
        }

        int len = typedCommand.size();
        if(auto result = nlohmann::json::object();len < 2) {
            arl::info("incorrect command typed");
            continue;
        }
        else if(len == 2 && typedCommand[1] == "HELLO") {
            result["id"] = 1;
            result["command"] = "HELLO";
            sendMessage(result.dump());
        }
        else if(len == 4 && typedCommand[1] == "login") {
            result["id"] = 1;
            result["command"] = "login";
            result["login"] = typedCommand[2];
            result["password"] = typedCommand[3];
            sendMessage(result.dump());
        }
        else if(len == 3 && typedCommand[1] == "message") {
            result["id"] = 1;
            result["command"] = "message";
            result["login"] = typedCommand[2];
            result["session"] = m_sessionId;
            sendMessage(result.dump());
        }
        else if(len == 3 && typedCommand[1] == "ping") {
            result["id"] = 1;
            result["command"] = "ping";
            result["session"] = m_sessionId;
            sendMessage(result.dump());
        }
        else if(len == 3 && typedCommand[1] == "logout") {
            result["id"] = 1;
            result["command"] = "logout";
            result["session"] = m_sessionId;
            sendMessage(result.dump());
        }
        else
            arl::info("incorrect command typed");
    }while(input!= "q");
}
