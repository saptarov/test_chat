#include <iostream>
#include "Client.h"
bool DEBUG_CHAT = true;

int main(int argc, char **argv) {

    if(AClient cli;!DEBUG_CHAT)
    {
        if(cli.connToSrv(argv[1],std::stoi(argv[2])))
        {
            cli.run();
            cli.update();
        }
    }
    else
        {
            if(cli.connToSrv("127.0.0.1",9995))
            {
                cli.run();
                cli.update();
            }
        }
    return 0;
}
