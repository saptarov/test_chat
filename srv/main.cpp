#include <iostream>
#include "Server.h"
bool DEBUG_CHAT = true;

int main(int argc, char **argv)
{
    AServer srv;
    if(!DEBUG_CHAT) {
        if(srv.startServer(argv[1],std::stoi(argv[2]))) {
            srv.run();
            srv.update();
        }
    }
    else {
            if(srv.startServer("127.0.0.1",9995)) {
                srv.run();
            }
        }
    return 0;
}
