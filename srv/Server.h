#ifndef SERVER_H
#define SERVER_H
#include "NetManage.h"

struct SClientInfo
{
    bool authorized =false;
    std::string login;
    std::vector<std::string> clientMessages;
};

class AServer: public ANetManage
{
    std::map<int,SClientInfo> m_clients;
    std::mutex m_mutexAccept;
    std::mutex m_mutexHandler;
    std::map<uint,int> m_clientBySession;
public:
    inline static uint m_sessionId=0;
    AServer();
    /*
     * @brief started application threads
    */
    void run();
    /*
     * @brief wait recv data
     * @clientID   id of socket connected clients
    */
    void reader(int clientID);
    /*
     * @brief wait new connect
    */
    void waitClients();
    /*
     * @brief accept a new client
    */
    void acceptConnect(int clientId);
    /*
     * @brief -- recieved data handler
     * @clientID   id of socket connected clients
    */
    bool handler(int clientId);
    /*
     * @brief   registered our client in server
     * @input   typing command   required
    */
private:
    //login and password map
    std::map<std::string,std::string> m_registeredUsers =
    {
        {
            "user1","user1"
        },
        {
            "user2","user2"
        },
        {
            "user3","user3"
        },
        {
            "user4","user4"
        },
    };
};

#endif // SERVER_H
