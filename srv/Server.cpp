#include "Server.h"

AServer::AServer()
{

}
void AServer::run()
{
    arl::info("Server is running");
    waitClients();
}
void AServer::reader(int clientID)
{
    arl::info("Start read client {}",clientID);
    while(1) {
        readBuf(clientID,dataPack);
        if(!handler(clientID))
            arl::info("reciev incorret packed");
        update();
    }
}
void AServer::waitClients()
{
    fd_set master;				//File descriptor storing all the sockets.
    FD_ZERO(&master);			//Empty file file descriptor.

    FD_SET(tSocket, &master);		//Add listening socket to file descriptor.
    while(1) {
        struct sockaddr_in cli_addr;
        socklen_t clilen = sizeof(cli_addr);
        int client = accept(tSocket, (struct sockaddr *) &cli_addr, &clilen);
        if (client < 0) {
            arl_error("ERROR on accept");
            return;
        }
        acceptConnect(client);
    }
}
void AServer::acceptConnect(int clientId)
{
    std::lock_guard<std::mutex> l(m_mutexAccept);
    arl::info("accept client id {}",clientId);

    m_clients.emplace(clientId,SClientInfo());
    std::thread(&AServer::reader,this,clientId).detach();
}
bool AServer::handler(int clientId)
{
    std::lock_guard<std::mutex> l(m_mutexHandler);
    nlohmann::json j =  nlohmann::json::parse(dataPack.data());
    auto it = j.find("id");
    if(it!=j.end())
    {
        int id = it.value();
        if(auto result = nlohmann::json::object();!m_clients[clientId].authorized && id > 2) {
            if(!arl::isCorrectData({"id","command"},result))
                return arl_error("incorrect param");
            if(std::string command = j.find("command").value();command == "message") {
                result["id"] = 2;
                result["command"] = "message_reply";
                result["status"] = "failed";
                result["message"] = "you is not autorized";
            }
            else if(command == "ping"){
                result["id"] = 2;
                result["command"] = "ping_reply";
                result["status"] = "failed";
                result["message"] = "you is not autorized";
            }
            else
                return arl_error("unknown command");
            sendMessage(result.dump(),clientId);
        }
        else if(id == 1) {
            if(!arl::isCorrectData({"id","command"},result))
                return arl_error("incorrect HELLO param");
            result["id"] = 1;
            result["command"] = "HELLO";
            result["auth_method"] = "you need autentificated";
            sendMessage(result.dump(),clientId);
        }
        else if(id == 2) {
            if(!arl::isCorrectData({"id","command"},result))
                return arl_error("incorrect HELLO param");
            auto itCommand = j.find("command");
            if(std::string comm = itCommand.value(); comm == "login") {
                if(!arl::isCorrectData({"login","password"},result))
                    return arl_error("not have is login and password");
                if(m_registeredUsers.count(j.find("login").value())
                        && m_registeredUsers[j.find("login").value()] == j.find("password").value()
                )
                {
                    result["id"] = 2;
                    result["command"] = "login";
                    result["status"] = "ok";
                    result["session"] = ++AServer::m_sessionId;
                    m_clientBySession[AServer::m_sessionId] = clientId;
                    sendMessage(result.dump(),clientId);
                    m_clients[clientId].authorized = true;
                    m_clients[clientId].login = j.find("login").value();
                }
                else {
                    result["id"] = 2;
                    result["command"] = "ping_reply";
                    result["status"] = "failed";
                    result["message"] = "login or password is incorrected";
                    sendMessage(result.dump(),clientId);
                    m_clients[clientId].authorized = true;
                }
            }
            else if(comm == "message") {
                if(!arl::isCorrectData({"body","session"},result))
                    return arl_error("not have is body and session id");
                int sessId = j.find("session").value();
                if(m_clientBySession[sessId] != clientId)
                    return arl_error("client {} send incorrect session id {}",clientId,sessId);
                std::string mess_body = j.find("body").value();
                m_clients[clientId].clientMessages.push_back(mess_body);
                uint idOfMessage = m_clients[clientId].clientMessages.size();
                result["id"] = 2;
                result["command"] = "message_reply";
                result["status"] = "ok";
                result["client_id"] = idOfMessage;
                sendMessage(result.dump(),clientId);
                std::string &loginOfSender = m_clients[clientId].login;
                for(auto client : m_clients)
                {
                    if(client.second.authorized)
                    {
                        result["id"] = 2;
                        result["command"] = "message";
                        result["body"] = mess_body;
                        result["sender_login"] = loginOfSender;
                        result["session"] = AServer::m_sessionId;
                        sendMessage(result.dump(),client.first);
                    }
                }
            }
            else if(comm == "ping") {
                result["id"] = 2;
                result["command"] = "ping_reply";
                result["status"] = "ok";
                sendMessage(result.dump(),clientId);
            }
            else if(comm == "logout") {
                result["id"] = 2;
                result["command"] = "logout_reply";
                result["status"] = "ok";
                sendMessage(result.dump(),clientId);
            }
        }
        else {
            return arl_error("incorrect HELLO param");
        }
    }
    return true;
}
